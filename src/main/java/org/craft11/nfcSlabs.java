package org.craft11;

import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class nfcSlabs extends JavaPlugin {

    private static Material sandstoneMaterial = Material.getMaterial(24);
    private static ItemStack sandstoneStack = new ItemStack(sandstoneMaterial, 1);

    private static Material cobblestoneMaterial = Material.getMaterial(4);
    private static ItemStack cobblestoneStack = new ItemStack(cobblestoneMaterial, 1);



    private static Material bricksMaterial = Material.getMaterial(45);
    private static ItemStack bricksStack = new ItemStack(bricksMaterial, 1);

    private static Material woodMaterial = Material.getMaterial(5);
    private static ItemStack woodStack = new ItemStack(woodMaterial, 1);

    private ShapedRecipe sandstone = new ShapedRecipe(sandstoneStack);
    private ShapedRecipe cobblestone = new ShapedRecipe(cobblestoneStack);
    private ShapedRecipe bricks = new ShapedRecipe(bricksStack);
    private ShapedRecipe wood = new ShapedRecipe(woodStack);

    private Server server;


    @Override
    public void onDisable() {
        server.getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        server = this.getServer();
        PluginManager pm = server.getPluginManager();
        recipe();
    }

    private void recipe() {
        sandstone.shape("x","x").setIngredient('x', Material.getMaterial(44).getNewData((byte)1));
        wood.shape("x","x").setIngredient('x', Material.getMaterial(44).getNewData((byte)2));
        cobblestone.shape("x","x").setIngredient('x', Material.getMaterial(44).getNewData((byte)3));
        bricks.shape("x","x").setIngredient('x', Material.getMaterial(44).getNewData((byte)4));

        server.addRecipe(sandstone);
        server.addRecipe(wood);
        server.addRecipe(cobblestone);
        server.addRecipe(bricks);
    }
}
